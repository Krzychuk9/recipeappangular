import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    firebase.initializeApp({
      apiKey: 'AIzaSyDEErQrjNGhlKCOSxa869MKdTnEj9NwL4w',
      authDomain: 'ng-recipe-book-8d8d1.firebaseapp.com'
    });
  }
}
