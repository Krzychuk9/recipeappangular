import {Component, OnInit} from '@angular/core';
import {Recipe} from '../recipe.model';
import {RecipeService} from '../recipe.service';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipeInDetails: Recipe;
  id: number;
  constructor(private recipeService: RecipeService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.recipeInDetails = this.recipeService.getRecipeById(this.id);
      }
    );
  }

  toShoppingList() {
    this.recipeService.addIngredientsToShoppingList(this.recipeInDetails.ingredients);
  }

  onRecipeDelete() {
    this.recipeService.removeRecipe(this.id);
  }
}
