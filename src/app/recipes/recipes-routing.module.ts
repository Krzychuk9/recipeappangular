import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RecipeDetailComponent} from './recipe-detail/recipe-detail.component';
import {StartingComponentComponent} from './starting-component/starting-component.component';
import {RecipesComponent} from './recipes.component';
import {AuthGuard} from '../auth/auth-guard.service';
import {RecipeEditComponent} from './recipe-edit/recipe-edit.component';

const recipesRoutes: Routes = [
  {path: '', component: RecipesComponent, children: [
      {path: '', component: StartingComponentComponent},
      {path: 'new', component: RecipeEditComponent, canActivate: [AuthGuard]},
      {path: ':id', component: RecipeDetailComponent},
      {path: ':id/edit', component: RecipeEditComponent, canActivate: [AuthGuard]}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(recipesRoutes)],
  exports: [RouterModule]
})
export class RecipesRoutingModule {}
