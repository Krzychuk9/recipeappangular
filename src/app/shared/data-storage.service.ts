import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpRequest} from '@angular/common/http';
import {Recipe} from '../recipes/recipe.model';
import {RecipeService} from '../recipes/recipe.service';
import 'rxjs/add/operator/map';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class DataStorageService {
  constructor(private http: HttpClient, private recipeService: RecipeService, private auth: AuthService) {}
  storeData() {
    const recipes = this.recipeService.getRecipes();
    return this.http.put('https://ng-recipe-book-8d8d1.firebaseio.com/recipes.json', recipes, {
      observe: 'body',
    });
  }
  storeDataWithProgress() {
    const recipes = this.recipeService.getRecipes();
    const request = new HttpRequest('PUT', 'https://ng-recipe-book-8d8d1.firebaseio.com/recipes.json',
      recipes, {reportProgress: true});
    return this.http.request(request);
  }
  getData() {
    this.http.get<Recipe[]>('https://ng-recipe-book-8d8d1.firebaseio.com/recipes.json', {})
      .map(
        (recipes) => {
          for (const recipe of recipes) {
            if (!recipe['ingredients']) {
              recipe['ingredients'] = [];
            }
          }
          return recipes;
        }
      ).subscribe(
        (recipes: Recipe[]) => this.recipeService.updateRecipes(recipes)
      );
  }
}
