import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appDropdownClick]'
})
export class DropdownClickDirective {
  @HostBinding('class.open') isOpen = false;
  @HostListener('click') toggleOpen() {
    this.isOpen = !this.isOpen;
  }
}
