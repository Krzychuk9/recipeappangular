import {NgModule} from '@angular/core';
import {DropdownDirective} from './dropdown.directive';
import {CommonModule} from '@angular/common';
import {DropdownClickDirective} from './dropdownClick.directive';

@NgModule({
  declarations: [
    DropdownDirective,
    DropdownClickDirective
  ],
  exports: [
    CommonModule,
    DropdownDirective,
    DropdownClickDirective
  ]
})
export class SharedModule {}
